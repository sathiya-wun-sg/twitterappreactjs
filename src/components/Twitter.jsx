import React, { Component } from 'react';
import './Twitter.css';
import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { TwitterTimelineEmbed, TwitterShareButton, TwitterTweetEmbed } from 'react-twitter-embed';

firebase.initializeApp({
      apiKey: "AIzaSyBK4SCBgb6DeB2NMEhEXSHdpgFqstUArF4",
      authDomain: "fir-auth-b3fc7.firebaseapp.com"
})
class Twitter extends Component {
    state = { isSignedIn:false }
  uiConfig = { 
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.TwitterAuthProvider.PROVIDER_ID
    ],
    callbacks:{
      signInSuccess: () => false
    }

  }
  componentDidMount = () =>{   
    firebase.auth().onAuthStateChanged(user => {
      this.setState({isSignedIn: !!user})
      //console.log(firebase.auth().currentUser);
      //console.log(firebase.auth().currentUser.providerData[0]);
      //console.log(firebase.auth().currentUser.uid);
    })
  }
  render() {
    return (
      <div className="App">
        {this.state.isSignedIn ? (
          <span>
            <div className="signOut"><button onClick={() => firebase.auth().signOut()}><img alt="Profile" src={firebase.auth().currentUser.photoURL} /> Sign Out!</button></div>            
            <h1>Welcome {firebase.auth().currentUser.displayName}</h1>
            
            <h2>Display timeline or tweets {}</h2>
            <h2>Post tweet, Reply to a tweet, Retweet a tweet</h2>
            <div className="centerContent">
              <div className="selfCenter">              
                <TwitterTimelineEmbed sourceType="profile" userId={Number(firebase.auth().currentUser.providerData[0].uid)} noFooter autoHeight noScrollbar />
                <TwitterShareButton url={'https://twitter.com/'+firebase.auth().currentUser.uid} options={{ text: '#Your tweet here.', via: firebase.auth().currentUser.displayName }}  />
              </div>
            </div>
            <div className="centerContent">
              <div className="selfCenter">
              <TwitterTweetEmbed tweetId={firebase.auth().currentUser.uid} />
              </div>
            </div>
          </span>
        ):(
          <div className="siginpage">
            <div className="selfCenter"> 
                <h1>React Authenticate with Twitter OAuth</h1>
                <br></br>
                <h3>Please Sign In with Twitter Account</h3>
                <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
            </div>
          </div>
        )

        }
      </div>
    );
  }
}
 
export default Twitter;