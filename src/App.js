import React, { Component } from 'react';
import './App.css';
import Twitter from './components/Twitter';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Twitter></Twitter>
      </div>
    );
  }
}

export default App;
